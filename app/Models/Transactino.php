<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transactino extends Model
{
 
    protected $fillable = [
        'invoice_no', 'route_id', 'route_no', 'total_amount'
    ];
}
