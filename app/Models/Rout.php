<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rout extends Model
{
    protected $table = 'route';
    protected $fillable = [
        'route_name', 'route_no', 'employee_id'
    ];
}
