<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController as BaseController;
use App\User;
use Validator;
use Illuminate\Database\DatabaseManager;
use App\Models\Rout;
use App\Models\Transactino;
use App\Models\Employee;
use Illuminate\Support\Facades\DB;
use App\Http\Repositories\EmployeeRepository;
use App\Http\Services\EmployeeService;

class EmployeeController extends BaseController
{
    private $db;
    private $employee;
    private $rout;
    private $transactino;
    private $employeeRepository;
    private $employeeService;


    public function __construct(
        DatabaseManager $db,
        Employee $employee,
        Rout $rout,
        Transactino $transactino,
        EmployeeRepository $employeeRepository,
        EmployeeService $employeeService

    ) {
        $this->db = $db;
        $this->employee = $employee;
        $this->rout = $rout;
        $this->transactino = $transactino;
        $this->employeeRepository = $employeeRepository;
        $this->employeeService = $employeeService;
    }

    /**
     * Insert users
     *
     */
    public function insertEmployee(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'first_name' => 'required',
            'last_name' => 'required',

        ]);

        if ($validator->fails()) {

            return $this->sendError('Validation Error.', $validator->errors());
        }
        $employeeInsert = $this->employeeService->insertEmployee($request);

        return $this->sendResponse($employeeInsert, 'success');
    }

    /**
     * Insert route
     *
     */
    public function createRoute(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'user_id' => 'required',
        ]);

        if ($validator->fails()) {

            return $this->sendError('Validation Error.', $validator->errors());
        }
        $routeName = $this->employeeService->getEmployee($request['user_id']);
        $routeInsertId = $this->employeeService->insertRoute($routeName[0]->first_name, $request);

        return $this->sendResponse($routeInsertId, 'success');
    }

    /**
     * Create Invoice
     *
     */
    public function createInvoice(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'route_id' => 'required',
            'total_amount'=>'required'
        ]);

        if ($validator->fails()) {

            $errors = $validator->errors()->all();

            return $this->sendError($errors, 400);
        }

        $routeNo = $this->employeeService->getRouteDetails($request['route_id']);

        $transactionInsert = $this->transactino->create([
            'route_id' => $request['route_id'],
            'route_no' => $routeNo[0]->route_no,
            'total_amount' => $request['total_amount'],
        ]);

        return $this->sendResponse($transactionInsert, 'success');
    }

    /**
     * get Invoice
     *
     */
    public function getInvoiceDetails(Request $request)
    {
        if ($request['id']) {
            

            $id = $request['id'];

            $details = $this->employeeService->getEmployeeDetailsBased($id);
            return $this->sendResponse($details, 'success');
        } else {
           
            $details = $this->employeeService->getWholeEmployee();
            return $this->sendResponse($details, 'success');
        }
    }
}
