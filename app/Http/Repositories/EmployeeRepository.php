<?php

namespace App\Http\Repositories;

use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Facades\DB;
use App\Models\Rout;
use App\Models\Transactino;
use App\Models\Employee;

class EmployeeRepository
{
    private $db;
    private $rout;
    private $employee;
    private $transactino;

    public function __construct(
        DatabaseManager $db,
        Rout $rout,
        Transactino $transactino,
        Employee $employee
    ) {
        $this->db = $db;
        $this->rout = $rout;
        $this->transactino = $transactino;
        $this->employee = $employee;
    }

    /**
     * inserting employee
     *
     * @param  $request
     * @return object
     */
    public function insertEmployeeDetails($request)
    {

        $employeeInsert = $this->employee->create([
            'first_name'  => $request['first_name'],
            'last_name' => $request['last_name'],
        ]);

        return $employeeInsert;
    }

    /**
     * get employee details
     *
     * @param int $id
     * @return object
     */
    public function getEmployeeDetailsById($id)
    {
        return $this->employee->select('first_name')->where('id', $id)->get();
    }

    /**
     * inserting route
     *
     * @param  $route_name
     * @param  $request
     * @return object
     */
    public function insertRouteDetails($route_name, $request)
    {
        return $this->rout->create([
            'route_name'  => $route_name,
            'route_no' => $request['route_no'],
            'employee_id' => $request['user_id']
        ]);
    }

    /**
     * get route details
     *
     * @param int $route_id
     * @return object
     */
    public function getRouteDetailsById($id)
    {
        return $this->rout->select('route_no')->where('id', $id)->get();
    }

    /**
     * get invoice details
     *
     * @param [type] $id
     * @return void
     */
    public function getDetailsBasedId($id)
    {
        return $this->rout
            ->join('transactinos', 'transactinos.route_id', '=', 'route.id')
            ->select('transactinos.route_id', 'route.route_name', 'route.route_no', DB::raw('SUM(transactinos.total_amount) AS total_amount'))
            ->where('route.id', $id)
            ->groupBy('transactinos.route_id')
            ->get();
    }

    /**
     * get invoice details of whole employee
     *
     * @return object
     */
    public function getWholeEmployeeDetails()
    {
        return $this->rout
            ->leftJoin('transactinos', 'transactinos.route_id', '=', 'route.id')
            ->select('transactinos.route_id','route.id AS route_ids', 'route.route_name', 'route.route_no', DB::raw('SUM(transactinos.total_amount) AS total_amount'))
            ->groupBy('transactinos.route_id')
            ->get();
    }
}
