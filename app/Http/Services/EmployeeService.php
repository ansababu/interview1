<?php

namespace App\Http\Services;

use App\Http\Repositories\EmployeeRepository;
use Illuminate\Database\DatabaseManager;
use App\User;
use Carbon\Carbon;

class EmployeeService
{
    private $employeeRepository;
    private $db;

    public function __construct(
        DatabaseManager $db,
        EmployeeRepository $employeeRepository
    ) {
        $this->db = $db;
        $this->employeeRepository = $employeeRepository;
    }

    /**
     *Inserting employee
     *
     * @param  $request
     * @return object
     */
    public function insertEmployee($request)
    {
        $employeeInsertDetails = $this->employeeRepository->insertEmployeeDetails($request);

        return $employeeInsertDetails;
    }

    /**
     * getEmployee details
     *
     * @param int $id
     * @return object
     */
    public function getEmployee($id)
    {
        $employeeDetails = $this->employeeRepository->getEmployeeDetailsById($id);

        return $employeeDetails;
    }

    /**
     * inserting route
     *
     * @param  $route_name
     * @param  $request
     * @return object
     */
    public function insertRoute($route_name, $request)
    {
        return $this->employeeRepository->insertRouteDetails($route_name, $request);
    }

    /**
     * get route details
     *
     * @param int $route_id
     * @return object
     */
    public function getRouteDetails($route_id)
    {
        return $this->employeeRepository->getRouteDetailsById($route_id);
    }

    /**
     * get invoice details
     *
     * @param [type] $id
     * @return void
     */
    public function getEmployeeDetailsBased($id)
    {
        $result = $this->employeeRepository->getDetailsBasedId($id);

        $results = [
            'salesman' => [
                'route_id' => $result[0]->route_id,
                'route_name' => $result[0]->route_name,
            ],

            'salesman_summary' => [
                'total_amount' => $result[0]->total_amount,
            ],
        ];

        return $results;
    }

    /**
     * get invoice details of whole employee
     *
     * @return object
     */
    public function getWholeEmployee()
    {
        $result = $this->employeeRepository->getWholeEmployeeDetails();


        foreach ($result as $value) {

            if ($value->total_amount == '') {
                $totalAmount = 0;
            } else {
                $totalAmount = $value->total_amount;
            }

            $results[] = [
                'salesman' => [
                    'route_id' => $value->route_ids,
                    'route_name' => $value->route_name,
                ],

                'salesman_summary' => [
                    'total_amount' => $totalAmount,
                ],
            ];
        }

        return $results;
    }
}
